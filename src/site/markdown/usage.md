# Usage

There are three different ways to get an access token from the central Identity
Provider (IdP). This guide shows how you can use them.

## 1. Your client is a registered client in the IdP

In this workflow you have the following values at hand:

- your client ID, a random string that is public knowledge
- your client secret, a random string that you should keep secret, as the
    name suggests
- the public key from the IdP you are using. Each IdP instance must use a
  different private/public key pair.

In this workflow you allow certain or all users to use your client. Your client
does not need a login page, instead it uses the login page from the IdP. You
can generate the link to the Login page of the IdP using this library:

```java

OAuth2Client config = new OAuth2Client();
config.setHost("https://dev02.imbei.uni-mainz.de/auth");
config.setClientId("your-client-id");

String linkUrl = OAuth2ClientConfig.getRedirectUrl(OAuth2Client config, String scheme,
            String serverName, int port, String contextPath, String redirectUrl,
            Scope.OPENID);

```

Where `scheme, serverName, port and contextPath` are value from the request, whereas
`redirectUrl` is a URL in your application, e.g. "/samplyLogin.xhtml".

This method will generate a URL like this:

```
https://dev02.imbei.uni-mainz.de/auth/grant.xhtml?client_id=your-client-id&scope=openid&redirect_uri=https%3A%2F%2Fyour-host%2FsamplyLogin.xhtml
```

If the user click on it he will see a login page, if he didn't login yet. After he has
logged in he will be redirected back to your web application with an additional request
parameter, the code:

```
https://your-host/samplyLogin.xhtml?code=a-random-code
```

Your application can now use this code in combination with your client ID and client secret
to get an access token, ID token and refresh token. All tokens have a limited lifetime and a different purpose:

- use the access token to access other applications via their respective REST interface
- use the ID token to identify the user, e.g. get his real name
- use the refresh token to renew your access token

You can use this library to make the final call:

```
AuthClient client = new AuthClient(authUrl, publicKey, "your-client-id",
    "your-client-secret", "a-random-code", client);
JWTAccessToken accessToken = client.getAccessToken();
JWTIDToken idToken = client.getIDToken();
```

This method will check if the tokens are valid and so on, and return the access token
if everything is fine. You can use this client further to search for users:

```
UserListDTO userList = client.searchUser("John");
List<UserDTO> users = userList.getUsers();
```

# 2. Your client has a private key

In this case your client always acts on behalf of exactly one user. This user is
always the same and has registered a public key in the IdP. In this case you need
to sign a random code with your private key in order to get an access token (and only
an access token).

Use the AuthClient to get a new access token:

```
AuthClient client = new AuthClient(authUrl, publicKey,
    yourPrivateKey, client);
JWTAccessToken token = client.getAccessToken();
```
Using this workflow will never return an ID token or refresh token
(because it does not make much sense to tell your application about yourself and
you already have the option to get a new access token without a refresh token).


# 3. You already have a refresh token

All you need to do is to initialize the AuthClient with this refresh token
and get a new access token:

```
AuthClient client = new AuthClient(authUrl, publicKey,
    accessToken (or null), idToken (or null),
    refreshToken, client);
JWTAccessToken token = client.getAccessToken();
```

If you already have an access token you can give it to the AuthClient. Upon
requesting an access token, the client will check if the old access token is still
valid. If it is not valid any longer, it will request a new one with the
refresh token. The access token and refresh token are optional in this case.
